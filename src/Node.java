public class Node {
    public int age;
    public Node next;
    public Node previous;
    Node(int age){
        this.age = age;
    }
    public void Display(){
        System.out.println("Age is "+age);
    }
}
