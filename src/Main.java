import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {

        boolean isContinue = true;

        DoublyLinkedList list = new DoublyLinkedList();

        Scanner scanner = new Scanner(System.in);

        while (isContinue) {
            System.out.println("[c]reate");
            System.out.println("[d]elete");
            System.out.println("[s]how");
            System.out.println("[e]xit");
            switch (scanner.next().charAt(0)) {
                case 'c': {
                    System.out.print("Enter age: ");
                    list.InsertFirst(scanner.nextInt());
                    break;
                }
                case 'd': {
                    System.out.print("Enter age for delete: ");
                    list.Delete(scanner.nextInt());
                    break;
                }
                case 's': {
                    list.show();
                    break;
                }
                case 'e': {
                    isContinue = false;
                    break;
                }
            }
        }

    }
}
