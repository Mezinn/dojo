public class DoublyLinkedList {
    public Node first;
    public Node last;

    DoublyLinkedList() {
        first = null;
        last = null;
    }

    public boolean isEmpty() {
        return first == null;
    }

    public void InsertFirst(int age) {
        Node temp = new Node(age);
        if (isEmpty()) {
            this.last = this.first = temp;
        } else {
            first.previous = temp;
            temp.next = first;
            first = temp;
        }
    }

    public Node Search(int age) {
        Node temp = this.first;
        while (temp != null) {
            if (age == temp.age) {
                return temp;
            }
            temp = temp.next;
        }
        return null;
    }

    public Node Delete(int key) {
        Node current = first;
        while (current.age != key) {
            current = current.next;
            if (current == null)
                return null;
        }
        if (current == first)
            first = current.next;
        else
// Удаление элемента с заданным ключом
// (предполагается, что список не пуст)
// От начала списка
// Пока не будет найдено совпадение
// Переход к следующему элементу
// Ключ не найден
// Ключ найден; это первый элемент?
// first --> старое значение next
// Не первый элемент
// старое значение previous --> старое значение next
            current.previous.next = current.next;
        if (current == last)
            last = current.previous;
// Последний элемент?
// старое значение previous <-- last
// Не последний элемент
        else
            // Старое значение previous <-- старое значение next
            current.next.previous = current.previous;
        return current;

    }

    public void show() {
        Node temp = this.first;
        while (temp != null) {
            temp.Display();
            temp = temp.next;
        }
    }

}
